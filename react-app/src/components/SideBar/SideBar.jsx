import "../SideBar/SideBar.css";
import Ads from "../Ads/Ads";

import { addProduct, removeProduct } from "../CartReducer/CartReducer";
import { addLike, removeLike } from "../LikeReducer/LikeReducer";
import { useDispatch, useSelector } from "react-redux";

import React,{ useState } from "react";

const SideBar = () => {
  const [activeCart, setActiveCart] = useState();
  const [activeLike, setActiveLike] = useState();
  const [value, setValue] = useState("Добавить в корзину");

  const count = useSelector((store) => store.cart.products.length);
  const countLike = useSelector((store) => store.like.likes.length);
  const dispatch = useDispatch();

  const handleProduct = () => {
    setActiveCart(!activeCart);
    if (value === "Добавить в корзину") {
      const addCart = addProduct({ id: 0, name: "Iphone 13" }); // создали действие
      dispatch(addCart); // Отправили действие  }
      setValue("Товар уже в корзине");
    } else {
      const removeCart = removeProduct({ id: 0, name: "Iphone 13" }); // создали действие
      dispatch(removeCart);
      setValue("Добавить в корзину");
    }
  };

  const handleLikeProduct = () => {
    setActiveLike(!activeLike);
    if (!activeLike) {
      const addFavorites = addLike({ id: 0, name: "Iphone 13" }); // создали действие
      dispatch(addFavorites); // Отправили действие
    } else {
      const removeLikeProduct = removeLike({ id: 0, name: "Iphone 13" }); // создали действие
      dispatch(removeLikeProduct);
    }
  };
  return (
    <>
      <aside className="sidebar">
        <div className="sidebar-widget">
          <div className="sidebar-widget__cost">
            <div className="sidebar-widget__last-cost-sell">
              <h3 className="sidebar-widget__last-cost">
                <b>
                  <s>75 990₽</s>
                </b>
              </h3>
              <div className="sidebar-widget__sell">
                <p className="sidebar-widget__percent sidebar-widget__percent_margin_0">
                  -8%
                </p>
              </div>
              <svg
                className={`${
                  countLike === 0
                    ? "sidebar-widget__icon color-svg"
                    : "sidebar-widget__icon-active"
                }`}
                viewBox="0 0 44 35"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                onClick={handleLikeProduct}
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M3.30838 2.95447C7.29788 -0.875449 13.7444 -0.875449 17.7339 2.95447L22.0002 7.05027L26.2667 2.95447C30.2563 -0.875449 36.7027 -0.875449 40.6923 2.95447C44.6817 6.78439 44.6817 12.973 40.6923 16.803L22.0002 34.7472L3.30838 16.803C-0.681121 12.973 -0.681121 6.78439 3.30838 2.95447ZM14.7876 5.78289C12.4253 3.51507 8.61698 3.51507 6.25465 5.78289C3.89234 8.05071 3.89234 11.7067 6.25465 13.9746L22.0002 29.0904L37.746 13.9746C40.1083 11.7067 40.1083 8.05071 37.746 5.78289C35.3838 3.51507 31.5754 3.51507 29.2131 5.78289L22.0002 12.7072L14.7876 5.78289Z"
                />
              </svg>
            </div>
            <h2 className="sidebar-widget__current-price margin-one">
              67 990₽
            </h2>
          </div>
          <div className="sidebar-widget__delivery">
            <p className="sidebar-widget__text margin-three">
              Самовывоз в четверг, 1 сентября — <b>бесплатно</b>
            </p>
            <p className="sidebar-widget__text margin-four">
              Курьером в четверг, 1 сентября — <b>бесплатно</b>
            </p>
          </div>
          <button
            className={`${
              count === 0
                ? "sidebar-widget__button background-color-dark-carrot background-click"
                : "sidebar-widget__button-gray"
            }`}
            id="buy"
            onClick={handleProduct}
          >
            {value}
          </button>
        </div>
        <div className="iframe">
          {/* <p className="iframe__heading">Реклама</p> */}
          <div className="iframe__ad-block">
          <Ads />
          </div>
          {/* <div className="iframe__ad-block">
            <Ads />
          </div> */}
        </div>
      </aside>
    </>
  );
};

export default SideBar;

