import { createSlice } from "@reduxjs/toolkit";

export const likeSlice = createSlice({
  name: "like",
  // Начальное состояние хранилища, товаров нет
  initialState: {
    likes: [],
  },

  reducers: {
    // Добавить товар, первый параметр это текущее состояние
    // А второй параметр имеет данные для действия
    addLike: (prevState, action) => {
      const like = action.payload;
      const hasInLike = prevState.likes.some(
        (prevLike) => prevLike.id === like.id
      );
      if (hasInLike) return prevState;
      return {
        ...prevState,
        // Внутри action.payload информация о добавленном товаре
        // Возвращаем новый массив товаров вместе с добавленным
        likes: [...prevState.likes, like],
      };
    },
    removeLike: (prevState, action) => {
      const like = action.payload;
      return {
        ...prevState,
        likes: prevState.likes.filter((prevLike) => {
          return prevLike.id !== like.id;
        }),
      };
    },
  },
});
// Экспортируем наружу все действия
export const { addLike, removeLike } = likeSlice.actions;
// И сам редуктор тоже
export default likeSlice.reducer;
