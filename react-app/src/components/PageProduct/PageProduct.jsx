import './PageProduct.css';
import Ads from '../Ads/Ads';
import ColorButton from '../Colors/ColorButton';
import ConfigButton from '../Configs/ConfigButton';
import Reviews from '../Reviews/Reviews';
import Form from '../Form/Form';
import SideBar from '../SideBar/SideBar';


function PageProduct() {
    return (
        <div className="container">
            <nav>
                <div className="navigation">
                        <a className="navigation__link" href="#">Электроника</a>
                        <span>&gt;</span>
                        <a className="navigation__link" href="#">Смартфоны и гаджеты</a>
                        <span>&gt;</span>
                        <a className="navigation__link" href="#"> Мобильные телефоны</a>
                        <span>&gt;</span>
                        <a className="navigation__link" href="#">Apple</a>
                </div>
            </nav>
            <main>
                <h2 className="title">Смартфон Apple iPhone 13, синий</h2>
                <div className="gallery">
                    <img className="gallery__img" src="img/image-1.webp" alt="айфон"/>
                    <img className="gallery__img" src="img/image-2.webp" alt="айфон"/>
                    <img className="gallery__img" src="img/image-3.webp" alt="айфон"/>
                    <img className="gallery__img" src="img/image-4.webp" alt="айфон"/>
                    <img className="gallery__img" src="img/image-5.webp" alt="айфон"/>
                </div>
                <div className="information">
                    <div className="information__main">
                        <section>
                            <h3 className="subtitle">Цвет товара: cиний</h3>
                            <ColorButton />
                            {/* <div className="product-selection">
                                <div className="product-selection__label">
                                    <input type="radio" id="radio-1" name="radio"/>
                                    <label htmlFor="radio-1"><img className="product-selection__img" src="img/color-1.webp" alt="айфон красного цвета"/></label>
                                </div>
                                <div className="product-selection__label">
                                    <input type="radio" id="radio-2" name="radio"/>
                                    <label htmlFor="radio-2"><img className="product-selection__img product-selection__img_size" src="img/color-2.webp" alt="айфон зеленого цвета"/></label>
                                </div>
                                <div className="product-selection__label">
                                    <input type="radio" id="radio-3" name="radio"/>
                                    <label htmlFor="radio-3"><img className="product-selection__img" src="img/color-3.webp" alt="айфон розового цвета"/></label>
                                </div>
                                <div className="product-selection__label">
                                    <input type="radio" id="radio-4" name="radio" checked/>
                                    <label htmlFor="radio-4"><img className="product-selection__img" src="img/color-4.webp" alt="айфон синего цвета"/></label>
                                </div>
                                <div className="product-selection__label">
                                    <input type="radio" id="radio-5" name="radio"/>
                                    <label htmlFor="radio-5"><img className="product-selection__img" src="img/color-5.webp" alt="айфон голубого цвета"/></label>
                                </div>
                                <div className="product-selection__label">
                                    <input type="radio" id="radio-6" name="radio"/>
                                    <label htmlFor="radio-6"><img className="product-selection__img" src="img/color-6.webp" alt="айфон черного цвета"/></label>
                                </div>
                            </div> */}
                            <h3 className="subtitle">Конфигурация памяти: 128 ГБ</h3>
                            <ConfigButton />
                            {/* <div className="buttons">
                                <div className="form_radio_btn ">
                                    <input type="radio" id="radio-7" name="radio-1" checked/>
                                    <label htmlFor="radio-7">128 ГБ</label>
                                </div>
                                <div className="form_radio_btn">
                                    <input type="radio" id="radio-8" name="radio-1"/>
                                    <label htmlFor="radio-8">256 ГБ</label>
                                </div>
                                <div className="form_radio_btn">
                                    <input type="radio" id="radio-9" name="radio-1"/>
                                    <label htmlFor="radio-9">512 ГБ</label>
                                </div>
                            </div> */}
                            <h3 className="subtitle">Характеристики товара</h3>
                            <ul className="list">
                                <li className="list__item">Экран: <b>6.1</b></li>
                                <li className="list__item">Встроенная память: <b>128 ГБ</b></li>
                                <li className="list__item">Операционная система: <b>iOS 15</b></li>
                                <li className="list__item">Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi</b></li>
                                <li className="list__item">Процессор: <a className="navigation__link" href="https://ru.wikipedia.org/wiki/Apple_A15" target="_blank"><b>Apple A15 Bionic</b></a></li>
                                <li className="list__item">Вес: <b>173 г</b></li>
                            </ul>
                        </section>
                        <section>
                            <h3 className="subtitle">Описание</h3>
                            <p className="text">Наша самая совершенная система двух камер.<br/>Особый взгляд на прочность дисплея.<br/>Чип, с которым всё супербыстро. <br/>Аккумулятор держится заметно дольше.<br/><i>iPhone 13 - сильный мира всего.</i></p>
                            <p className="text">Мы разработали совершенно новую схему расположения и развернули объективы на 45 градусов. Благодаря этому внутри корпуса поместилась наша лучшая система двухкамер с увеличенной матрицей широкоугольной камеры. Кроме того, мы освободилиместо для системы оптической стабилизации изображения сдвигом матрицы. Иповысили скорость работы матрицы на сверхширокоугольной камере.</p>
                            <p className="text">Новая сверхширокоугольная камера видит больше деталей в тёмных областяхснимков. Новая широкоугольная камера улавливает на 47% больше света для болеекачественных фотографий и видео. Новая оптическая стабилизация со сдвигомматрицы обеспечит чёткие кадры даже в неустойчивом положении.</p>
                            <p className="text">Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещенияфокуса и изменения резкости. Просто начните запись видео. Режим «Киноэффект» будет удерживать фокус на объекте съёмки, создавая красивый эффект размытиявокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести фокус на другогочеловека или объект, который появился в кадре. Теперь ваши видео будут смотретьсякак настоящее кино.</p>
                        </section>
                        <section className="table__information">
                            <p className="subtitle">Сравнение моделей</p>
                            <table className="table">
                                <thead className="table__thead">
                                    <tr>
                                        <th className="table__cell">Модель</th>
                                        <th className="table__cell">Вес</th>
                                        <th className="table__cell">Высота</th>
                                        <th className="table__cell">Ширина</th>
                                        <th className="table__cell">Толщина</th>
                                        <th className="table__cell">Чип</th>
                                        <th className="table__cell">Объём памяти</th>
                                        <th className="table__cell">Аккумулятор</th>
                                    </tr>
                                </thead>
                                <tbody className="table__td">
                                    <tr className="table__row">
                                        <td className="table__cell">iPhone 11</td>
                                        <td className="table__cell">194 грамма</td>
                                        <td className="table__cell">150.9 мм</td>
                                        <td className="table__cell">75.7 мм</td>
                                        <td className="table__cell">8.3 мм</td>
                                        <td className="table__cell">A13 Bionic chip</td>
                                        <td className="table__cell">до 128 Гб</td>
                                        <td className="table__cell">до 17 часов</td>
                                    </tr>
                                    <tr className="table__row">
                                        <td className="table__cell">iPhone 12</td>
                                        <td className="table__cell">164 грамма</td>
                                        <td className="table__cell">146.7 мм</td>
                                        <td className="table__cell">71.5 мм</td>
                                        <td className="table__cell">7.4 мм</td>
                                        <td className="table__cell">A14 Bionic chip</td>
                                        <td className="table__cell">до 256 Гб</td>
                                        <td className="table__cell">до 19 часов</td>
                                    </tr>
                                    <tr className="table__row">
                                        <td className="table__cell">iPhone 13</td>
                                        <td className="table__cell">174 грамма</td>
                                        <td className="table__cell">146.7 мм</td>
                                        <td className="table__cell">71.5 мм</td>
                                        <td className="table__cell">7.65 мм</td>
                                        <td className="table__cell">A15 Bionic chip</td>
                                        <td className="table__cell">до 512 Гб</td>
                                        <td className="table__cell">до 19 часов</td>
                                    </tr>
                                </tbody>
                            </table>
                        </section>
                        <div className="header-reviews">
                            <div className="header-reviews__amount">
                                <p className="header-reviews__reviews">Отзывы</p>
                                <span className="header-reviews__quantity">425</span>
                            </div>
                        </div>
                        <Reviews />
                        {/* <div className="user">
                            <div className="user__feedback">
                                <img className="user__img" src="img/review-1.jpeg" alt="фото автора"/>
                            </div>
                            <div className="user__rating">
                                <div className="user__product-evaluation">
                                    <p className="user__name">Марк Г.</p>
                                    <div className="user__stars">
                                        <img  className="user__stars-img" src="img/Звезда.svg" alt="оценка"/>
                                        <img  className="user__stars-img" src="img/Звезда.svg" alt="оценка"/>
                                        <img  className="user__stars-img" src="img/Звезда.svg" alt="оценка"/>
                                        <img  className="user__stars-img" src="img/Звезда.svg" alt="оценка"/>
                                        <img  className="user__stars-img" src="img/Звезда.svg" alt="оценка"/>
                                    </div>
                                </div>
                                <div className="user__opinion">
                                    <p className="user__opinion-text"> <b>Опыт использования:</b>  менее месяца</p>
                                    <p className="user__opinion-text"><b>Достоинства:</b><br/>это мой первый айфон после после огромного количества телефонов на андроиде. всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая, ширик тоже на высоте.</p>
                                    <p className="user__opinion-text"><b>Недостатки:</b><br/>к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь) а если нужно переносить фото с компа, то это только через itunes, который урезает качество фотографий исходное</p>
                                    <div>
                                        <hr className="separator"/>
                                    </div>
                                </div> 
                            </div>
                        </div> */}
                        {/* <div className="user">
                            <div className="user__feedback">
                                <img className="user__img" src="img/review-2.jpeg" alt="фото автора"/>
                            </div>
                            <div className="user__rating">
                                <div className="user__product-evaluation">
                                    <p className="user__name">Валерий Коваленко</p>
                                    <div className="user__stars">
                                        <img  className="user__stars-img" src="img/Звезда.svg" alt="оценка"/>
                                        <img  className="user__stars-img" src="img/Звезда.svg" alt="оценка"/>
                                        <img  className="user__stars-img" src="img/Звезда.svg" alt="оценка"/>
                                        <img  className="user__stars-img" src="img/Звезда.svg" alt="оценка"/>
                                        <img  className="user__stars-img" src="img/Звезда (1).svg" alt="оценка"/>
                                    </div>
                                </div>
                                <div className="user__opinion">
                                    <p className="user__opinion-text"> <b>Опыт использования:</b>  менее месяца</p>
                                    <p className="user__opinion-text"><b>Достоинства:</b><br/>OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго</p>
                                    <p className="user__opinion-text"><b>Недостатки:</b><br/>Плохая ремонтопригодность</p>
                                </div> 
                            </div>
                        </div>  */}
                        <Form />
                        {/* <form className="form">
                            <div className="form__information">
                                <p className="subtitle">Добавить свой отзыв</p>
                                <div className="form__input">
                                    <div className="form__withError">
                                        <input className="form__name input" type="text" id="name" placeholder="Имя и фамилия"/>
                                        <div className="form__error" id="error__name__requered">Вы забыли указать имя и фамилию</div>
                                        <div className="form__error" id="error__name__length">Имя не может быть короче двух символов</div>
                                    </div>
                                    <div className="form__withError">
                                        <input className="form__number input" type="text" min="1" max="5" step="1" placeholder="Оценка"/>
                                        <div className="form__error" id="error__grade">Оценка должна быть<br/>от 1 до 5</div>
                                    </div>
                                </div>
                                <textarea className="form__review input" name="text" id="text"  placeholder="Текст отзыва"></textarea>
                                <button className="btn form__btn" type="submit">Отправить отзыв</button>
                            </div>
                        </form> */}
                    </div>
                    <SideBar /> 
                     {/* <div className="aside"> 
                        <div className="aside__product-price">
                            <div className="aside__container">
                                <div className="aside__container-header">
                                    <div className="aside__container-price">
                                        <p className="aside__container-old-price">75 990</p>
                                        <p className="aside__container-discount">-8%</p>
                                    </div>
                                    <svg className="aside__heart" width="28" height="22" viewBox="0 0 28 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M2.78502 2.57269C5.17872 0.27474 9.04661 0.27474 11.4403 2.57269L14.0001 5.03017L16.56 2.57269C18.9537 0.27474 22.8216 0.27474 25.2154 2.57269C27.609 4.87064 27.609 8.5838 25.2154 10.8818L14.0001 21.6483L2.78502 10.8818C0.391321 8.5838 0.391321 4.87064 2.78502 2.57269ZM9.67253 4.26974C8.25515 2.90905 5.97018 2.90905 4.55278 4.26974C3.1354 5.63044 3.1354 7.82401 4.55278 9.18476L14.0001 18.2542L23.4476 9.18476C24.865 7.82401 24.865 5.63044 23.4476 4.26974C22.0302 2.90905 19.7452 2.90905 18.3279 4.26974L14.0001 8.42432L9.67253 4.26974Z"/></svg>
                                </div>
                                <p className="aside__container-new-price">67 990</p>
                                <p className="aside__container-pickup">Самовывоз в четверг, 1 сентября — <b>бесплатно</b></p>
                                <p className="aside__container-delivery">Курьером в четверг, 1 сентября — <b>бесплатно</b></p>
                                <button className="aside__button">Добавить в корзину</button>
                            </div>
                        </div>   */}
                        {/* <Ads />  */}
                    {/* </div>   */}
                </div>
            </main>
        </div>
    );
}

export default PageProduct;
