import "./Configs.css";
import configs from "./Configs";
import React, { useState } from "react";

function ConfigButton() {
  const [selectedVariant, setSelectedVariant] = useState('256');
  return (
    <div className="buttons">
      {configs.map((config) => {
        let actived = selectedVariant === config.value;
        let activeClass = actived ? "checked" : "";
        return (
          <div className={`form_radio_btn ${activeClass}`} key={config.value}>
            {/* вместо обработчика onChange надо было использовать onClick, ты же кликаешь на кнопку и это событие тебе надо ловить */}
            <input
              id={`color-button-${config.value}`}
              type="radio"
              name="radio-1"
              checked={actived}
              onChange={() => {}}
              onClick={() => setSelectedVariant(config.value)}
            />
            <label htmlFor={`color-button-${config.value}`}>
              {config.label}
              {/* а зачем тут было передавать activeClass?)) он тебе тут не нужен */}
              {/* {activeClass} */}
            </label>
          </div>
        );
      })}
    </div>
  );
}

export default ConfigButton;
