let configs = [
    {   value: '128',
        label: '128 ГБ',
    },
    {   value: '256',
        label: '256 ГБ',
    },
    {   value: '512',
        label: '512 ГБ',
    }
]

export default configs;