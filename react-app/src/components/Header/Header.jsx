import './Header.css';
import { useDispatch, useSelector } from "react-redux";

function Header() {
    const count = useSelector((store) => store.cart.products.length);
    const countLike = useSelector((store) => store.like.likes.length);
    return (
        <header>
            <div className="container">
                <h1 className="header">
                    <div>
                        <span><img className="header__logo" src="img/favicon.svg" alt="логитип проекта"/></span>
                        <span className="market">Мой</span>Маркет
                    </div>
                    <div className="header__counter">
                        <div className="header__heart">
                            <span className="header__liked">12</span>
                        </div>
                        <div className="header__basket">
                            {count>0 && (
                                <span className="header__products">{count}</span>
                            )}
                        </div>
                    </div>
                </h1>
            </div>
        </header> 
    );
}

export default Header;