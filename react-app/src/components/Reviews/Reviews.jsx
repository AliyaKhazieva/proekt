import './Reviews.css'
import reviews from './Data';
import reviewStars from './Reviews.stars';

function Reviews() {
    return (
        <div className="user">
            {reviews.map(function(review){
                return (
                    <div className="users" key={review.userName}>
                        <div className="user__feedback">
                            <img className="user__img" src={review.photo} alt=''/>
                        </div>
                        <div className="user__rating">
                            <div className="user__product-evaluation">
                                <p className="user__name">{review.userName}</p>
                                <div className="user__stars">
                                    {reviewStars.map(function(reviewStar, index){
                                        const isStarFilled = index +1 <= review.reviewRating
                                        return (
                                            <img key={reviewStar.id} className="user__stars-img" src={isStarFilled ? '/img/starEmpty.svg' : '/img/starFilled.svg'} alt={reviewStar.alt}/>
                                        )
                                    })}
                                </div>
                            </div>
                            <div className="user__opinion">
                                <p className="user__opinion-text"><b>Опыт использования:</b> {review.experienceTime}</p>
                                <p className="user__opinion-text"><b>Достоинства:</b><br/>{review.advantage}</p>
                                <p className="user__opinion-text"><b>Недостатки:</b><br/>{review.disadvantages}</p>
                                <div>
                                    <hr className="separator"/>
                                </div>
                            </div> 
                        </div>
                    </div>
                )

            })}
        </div> 
    )
}


export default Reviews;