import "../Form/Form.css"; 
import { useState, useEffect } from "react"; 
import React from "react"; 
 
const Form = () => { 
  const [name, setName] = useState(""); 
  const [rating, setRating] = useState(""); 
  const [text, setText] = useState(""); 
 
  const errorValue = { name: null, rating: null }; 
  const [errValue, setErrValue] = useState({ ...errorValue }); 
 
  const handleFocusName = (event) => { 
    setErrValue({ ...errorValue }); 
  }; 
  const handleFocusRating = (event) => { 
    setErrValue({ ...errorValue }); 
  }; 
 
  //сохраняю значения в localStorage 
  const handleName = (e) => { 
    setName(e.target.value); 
    localStorage.setItem("name", e.target.value); 
  }; 
  const handleRating = (e) => { 
    setRating(e.target.value); 
    localStorage.setItem("rating", e.target.value); 
  }; 
  const handleText = (e) => { 
    setText(e.target.value); 
    localStorage.setItem("text", e.target.value); 
  }; 
 
  useEffect(() => { 
    const getName = localStorage.getItem("name"); 
    setName(getName); 
 
    const getRating = localStorage.getItem("rating"); 
    setRating(getRating); 
 
    const getText = localStorage.getItem("text"); 
    setText(getText); 
 
    if (setName.length !== "") { 
      setName(getName); 
    } 
 
    if (setRating.length !== "") { 
      setRating(getRating); 
    } 
 
    if (setText.length !== "") { 
      setText(getText); 
    } 
  }, []); 
 
  const handleFormSubmit = (event) => { 
    event.preventDefault(); 
 
    const errorObj = {}; 
 
    if (name?.trim().length < 3) { 
      errorObj.name = "Имя не может быть менее 3-х символов!"; 
    } 
 
    // Выдает ошибку, если поле пустое 
    if (name === "") { 
      errorObj.name = "Вы забыли указать имя и фамилию!"; 
    }  
 
    if (rating <= 1 || rating > 5 || rating === "") { 
      errorObj.rating = "Оценка должна быть от 1 до 5!"; 
    } 
 
    if (errorObj.name && errorObj.rating) { 
      errorObj.rating = ""; 
    } 
 
    setErrValue(errorObj); 
 
    if (errorObj.name || errorObj.rating) { 
      return; 
    } else { 
      localStorage.setItem("name", ""); 
      setName(""); 
 
      localStorage.setItem("rating", ""); 
      setRating(""); 
 
      localStorage.setItem("text", ""); 
      setText(""); 
    } 
 
    // По клику на кнопку очищает поля в локал и стейт 
    localStorage.removeItem("name"); 
    setName(""); 
 
    localStorage.removeItem("rating"); 
    setRating(""); 
 
    localStorage.removeItem("text"); 
    setText(""); 
  }; 
 
  return ( 
    <form 
      className="block-form block-form_margin-left_170 block-form_margin-top_60" 
      id="form" 
      onSubmit={handleFormSubmit} 
    > 
      <legend className="block-form__title"> 
        <b>Добавить свой отзыв</b> 
      </legend> 
      <div className="block-form__input-form"> 
        <div className="block-form__input fields block-form_position"> 
          <input 
            type="text" 
            className="block-form__name hover-black input-field" 
            value={name} 
            onChange={handleName} 
            onFocus={handleFocusName} 
            name="name" 
            placeholder="Имя и фамилия" 
            id="name" 
          /> 
          <input 
            className="block-form__rating hover-black input-field" 
            name="number" 
            onChange={handleRating} 
            value={rating} 
            onFocus={handleFocusRating} 
            placeholder="Оценка" 
            id="rating" 
          /> 
        </div> 
 
        <div className="block-form__names-errors block-form_position"> 
          <div 
            className={`block-form__error-name ${ 
              errValue?.name ? "block-form__error-name_block" : "" 
            }`} 
            id="error-name" 
          > 
            {errValue?.name} 
          </div> 
          <div 
            className={`block-form__error-rating  ${ 
              errValue?.rating ? "block-form__error-rating_block" : "" 
            }`} 
            id="error-rating" 
          > 
            {errValue?.rating} 
          </div>
          </div> 
      </div> 
      <div> 
        <textarea 
          name="text-review" 
          className="block-form__text-review block-form__text-review_margin-top-bottom_30 hover-black input-field" 
          value={text} 
          onChange={handleText} 
          placeholder="Текст отзыва" 
          id="text-review" 
        ></textarea> 
      </div> 
      <button 
        className="block-form__button background-color-dark-carrot background-click" 
        type="submit" 
        id="form-button" 
        onClick={handleFormSubmit} 
      > 
        Отправить отзыв 
      </button> 
    </form> 
  ); 
}; 
 
export default Form;