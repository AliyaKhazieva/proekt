// Упражнение 1

let Usercount = +prompt('Введите число', '');

if (isNaN(Usercount)) {
    console.log('Вы ввели не число');
} else {
    let count = Usercount;

    let interval = setInterval(function() {
        console.log(`Осталось ${count}`);
        count = count - 1;


        if (count===0) {
            clearInterval(interval);
            console.log('Время вышло');
        }
    },1000)
}





// Упражнение 2

let promise = fetch('https://reqres.in/api/users');

promise
.then ((response) => {
    return response.json();
})
.then((result) => {
    console.log(`Получили пользователей ${result.data.length}:`);
    result.data.forEach((item) => {
        console.log(`-${item.first_name} ${item.last_name} (${item.email})`);
    });
});