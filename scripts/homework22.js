// Упражнение 1


let arr1= [1,2,10,5]

let arr2 = ["a", {},3,3, -2];

function getSumm(array) {

    let sum = 0;

    let arr = array.filter(function(v){
        if (typeof v === 'number') {
            return true;
        }
    });

    for (let i = 0; i < arr.length; i++) {
        sum = sum + arr[i];
    }
    return sum;

}

alert(getSumm(arr2));
alert(getSumm(arr1));



// Упраженение 3

let cart = new Set ([4884]);

function addToCart (product) {

    cart.add(product);
}

function removeFromCart (product) {

    cart.delete(product);
}

addToCart(3456)
console.log(cart);

addToCart(3456)
console.log(cart);

removeFromCart(4884)
console.log(cart);





let promise = new Promise(function(resolve) {

    // Вызываем функцию resolve через три секунды 

    setTimeout(function() {
        
        resolve("OK");
    
    },3000);
});

promise // Выполнится когда внутри обещания вызвали resolve

.then(function() {
    
    console.log("Обещание выполнено!");

})
